<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <section class="p-end--mv type2">
    <h2 class="p-end--ttl">
      <span class="en">ABOUT</span>
      <span class="jp">私たちについて</span>
    </h2>
  </section><!-- ./p-end--mv -->
  <div class="breadcrumbWrap type2">
    <div class="container">
      <div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li>私たちについて</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="p-end--cnt type2">
    <div class="p-about">
      <div class="p-about--intro">
        <div class="p-about--intro-infor">
          <div class="p-about--intro-infor-cnt">
            <h3 class="p-about--intro-ttl">
              <img class="" src="<?php echo $PATH;?>/assets/images/about/ttl.svg" alt="">
            </h3>
            <p class="desc mgb-10">近年、高層ビルが次々と建設されています。 古いビルが巨大ビル郡に変身していく姿は、まさに異次元空間に迷い込んだ錯覚すら覚えます。 急速に移り変る時代の中で、出来上がった建物をスラム化させることなく、清潔に美しく、未来永劫にわたって維持・管理するためのノウハウが強く求められています。 このような社会的要請にお応えするために、私たち協美社は「クリーン・アンド・セーフティー」をモットーとして、独創的な技術と、独自のパワーを存分に発揮。 その真価は多数の建造物で実証され、各方面から評価されております。 私たちが有する先天的なノウハウとエネルギッシュな実行力を、一度でもご活用いただけば…</p>
            <p class="desc">
              その成果について必ずやご満足いただけるものと、確信しております。 協美社が目指す「クリーン・アンド・セーフティー・ネットワーク」では、全ての業務が上質で密接な連携プレーをとりつつトータルシステムで目標達成に立ち向かいます。 私たちは、あくまでも総合的・全体的な一貫性を保持しつつお客様の信頼にお応えします。
            </p>
          </div>
          <div class="p-about--intro-infor-img">
            <img class="" src="<?php echo $PATH;?>/assets/images/about/profile.png" alt="">
            <div class="p-about--intro-infor-sign-inner sp-only mgt-50">
              <div class="p-about--intro-infor-sign-position">
                株式会社キョーエーメック 代表取締役社長
              </div>
              <div class="p-about--intro-infor-sign-mark">
                <img class="" src="<?php echo $PATH;?>/assets/images/about/mark.png" alt="">
              </div>
            </div>
          </div>
        </div>
        <div class="p-about--intro-infor-sign pc-only">
          <div class="p-about--intro-infor-sign-inner">
            <div class="p-about--intro-infor-sign-position">
              株式会社キョーエーメック 代表取締役社長
            </div>
            <div class="p-about--intro-infor-sign-mark">
              <img class="" src="<?php echo $PATH;?>/assets/images/about/mark.png" alt="">
            </div>
          </div>
        </div>
      </div>
      <div class="p-about--list">
        <div class="p-about--item">
          <div class="_inner">
            <div class="_cnt">
              <h4 class="p-about--item-ttl">夜に駆ける！深夜専門の清掃プロ集団！</h4>
              <p class="desc">2000年に深夜清掃専門部署を設立。1施設からスタートし、今では商業施設の共用部定期清掃・日常清掃40施設以上、テナント定期清掃700店舗以上、従業員200名以上に拡大。より綺麗に、より長く美しくをモットーに幾多の現場で経験した洗浄技術と10種類以上のコーティング剤で床を保護。落とせない汚れはありません。若年層が中心で広範囲の作業が可能となり生産性が高くコストパフォーマンスに優れています。</p>
            </div>
            <div class="p-about--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/about/about01.png" alt="">
            </div>
          </div>
        </div>
        <div class="p-about--item">
          <div class="_inner">
            <div class="_cnt">
              <h4 class="p-about--item-ttl">世界へ羽ばたく！海外拠点の人材を育成！</h4>
              <p class="desc">ベトナムのグループ会社からビルクリーニングの技能実習として人材を受け入れ、育成を強化しています。入国前からビルクリーニング業務の教育を実施。社内評価制度を設け、目標設定と昇給額を明確化しています。特定技能制度が始まってからは技能実習からの移行が進み、より責任のある仕事を任せています。生活面では環境の整った宿舎に加えて、ベトナム人の通訳を介した相談対応や病院等への同行体制を整え、不安の解消に努めています。</p>
            </div>
            <div class="p-about--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/about/about02.png" alt="">
            </div>
          </div>
        </div>
        <div class="p-about--item">
          <div class="_inner">
            <div class="_cnt">
              <h4 class="p-about--item-ttl">全力でビルを守る！知識経験豊富な設備部隊！</h4>
              <p class="desc">長年、ビル設備点検管理業務に携わる中で蓄積されたノウハウを、自営スタッフが修繕工事を実施しています。設備点検数は、170物件を超え、その内設備工事の実施は、110カ所に上ります。 お客様には、外注業者に依頼する時間を省きワンストップで工事を実施することで、安全性を確保し、尚且つスピーディに完工することを目指しています。</p>
            </div>
            <div class="p-about--item-thumb">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/about/about03.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>