<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <section class="p-end--mv">
    <h2 class="p-end--ttl">
      <span class="en">SERVICE</span>
      <span class="jp">事業内容</span>
    </h2>
  </section><!-- ./p-end--mv -->
  <div class="breadcrumbWrap">
    <div class="container">
      <div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li><a href="/">事業内容</a></li>
          <li><a href="/">建物総合管理事業</a></li>
          <li>日常清掃業務</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="p-end--cnt service">
    <div class="p-service">
      <div class="p-service--detail">
        <div class="container">
          <ul class="p-service--detail-nav">
            <li>
              <a class="active" href="">日常清掃業務</a>
            </li>
            <li>
              <a href="">深夜清掃業務</a>
            </li>
            <li>
              <a href="">設備保守業務</a>
            </li>
            <li>
              <a href="">駐車場業務</a>
            </li>
            <li>
              <a href="">警備業務</a>
            </li>
            <li>
              <a href="">受付業務</a>
            </li>
          </ul>
          <div class="p-service--detail-head">
            <div class="col2">
              <div class="col2-item">
                <h3 class="p-categories--ttl">建物総合管理事業</h3>
                <p class="desc3">日々の清掃の質が、施設の環境に大きく影響します。 扉の清掃や手すりの清掃を毎日行うことは、美観の維持のみならず衛生面の向上にもつながります。 また、当社では、作業の精度を高め維持する為の、インスペクションを定期的に行います。 見えない部分のチェックは手鏡を用いて確認を行い、高い部分の除塵確認、隙間部分の清掃状況の確認等、細部に渡るチェックを実施いたします。施設の特徴を考慮し、清掃方法や効率的な作業シフトをご提案することで、施設の美観維持をお約束いたします。</p>
              </div>
              <div class="col2-item">
                <div class="p-service--detail-head-thumb">
                  <img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/service/detail/detail01.png" alt="">
                </div>
              </div>
            </div>
          </div>
          <div class="p-service--detail-flow">
            <h3 class="p-service--subTtl">作業の流れ</h3>
            <div class="col4">
              <div class="p-service--detail-flow-item col4-item">
                <div class="col4-item--inner">
                  <p class="p-service--detail-flow-item-label">
                    <span>STEP 1</span>
                  </p>
                  <p class="p-service--detail-flow-item-ttl">作業ミーティング</p>
                </div>
              </div>
              <div class="p-service--detail-flow-item col4-item">
                <div class="col4-item--inner">
                  <p class="p-service--detail-flow-item-label">
                    <span>STEP 2</span>
                  </p>
                  <p class="p-service--detail-flow-item-ttl">鍵の受渡</p>
                </div>
              </div>
              <div class="p-service--detail-flow-item col4-item">
                <div class="col4-item--inner">
                  <p class="p-service--detail-flow-item-label">
                    <span>STEP 3</span>
                  </p>
                  <p class="p-service--detail-flow-item-ttl">フロア清掃</p>
                </div>
              </div>
              <div class="p-service--detail-flow-item col4-item">
                <div class="col4-item--inner">
                  <p class="p-service--detail-flow-item-label">
                    <span>STEP 4</span>
                  </p>
                  <p class="p-service--detail-flow-item-ttl">トイレ清掃</p>
                </div>
              </div>
            </div>
          </div>
          <div class="p-service--detail-content">
            <h3 class="p-service--subTtl">作業内容</h3>
            <div class="p-service--detail-content-row">
              <h4 class="p-service--detail-content-ttl">作業ミーティング</h4>
              <div class="col2-7">
                <div class="col2-7--left">
                  <p class="desc">作業ミーティングの説明文が入ります。作業ミーティングの説明文が入ります。作業ミーティングの説明文が入ります。作業ミーティングの説明文が入ります。作業ミーティングの説明文が入ります。作業ミーティングの説明文が入ります。</p>
                </div>
                <div class="col2-7--right">
                  <div class="thumb"><img class="cover border-radius-5" src="<?php echo $PATH;?>/assets/images/service/detail/detail02.png" alt=""></div>
                </div>
              </div>
            </div>
            <div class="p-service--detail-content-row">
              <h4 class="p-service--detail-content-ttl">鍵の受渡</h4>
              <div class="col2-7">
                <div class="col2-7--left">
                  <p class="desc">作業ミーティングの説明文が入ります。作業ミーティングの説明文が入ります。作業ミーティングの説明文が入ります。作業ミーティングの説明文が入ります。作業ミーティングの説明文が入ります。作業ミーティングの説明文が入ります。</p>
                </div>
                <div class="col2-7--right">
                  <div class="thumb"><img class="cover border-radius-5" src="<?php echo $PATH;?>/assets/images/service/detail/detail03.png" alt=""></div>
                </div>
              </div>
            </div>
            <div class="p-service--detail-content-row">
              <h4 class="p-service--detail-content-ttl">フロア清掃</h4>
              <div class="col2-7">
                <div class="col2-7--left">
                  <div class="col2-2">
                    <div class="col2-2-item">
                      <div class="col2-2-item--inner">
                        <h5 class="p-service--detail-content-subTtl">掃き清掃</h5>
                        <p class="desc">掃き掃除の説明文が入ります。掃き掃除の説明文が入ります。掃き掃除の説明文が入ります。掃き掃除の説明文が入ります。</p>
                      </div>
                    </div>
                    <div class="col2-2-item">
                      <div class="col2-2-item--inner">
                        <h5 class="p-service--detail-content-subTtl">説明文</h5>
                        <p class="desc">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                      </div>
                    </div>
                    <div class="col2-2-item">
                      <div class="col2-2-item--inner">
                        <h5 class="p-service--detail-content-subTtl">除塵作業(1)</h5>
                        <p class="desc">除塵作業の説明文が入ります。除塵作業の説明文が入ります。除塵作業の説明文が入ります。除塵作業の説明文が入ります。</p>
                      </div>
                    </div>
                    <div class="col2-2-item">
                      <div class="col2-2-item--inner">
                        <h5 class="p-service--detail-content-subTtl">説明文</h5>
                        <p class="desc">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                      </div>
                    </div>
                    <div class="col2-2-item">
                      <div class="col2-2-item--inner">
                        <h5 class="p-service--detail-content-subTtl">クリーナー掛け(1)</h5>
                        <p class="desc">クリーナー掛けの説明文が入ります。クリーナー掛けの説明文が入ります。クリーナー掛けの説明文が入ります。</p>
                      </div>
                    </div>
                    <div class="col2-2-item">
                      <div class="col2-2-item--inner">
                        <h5 class="p-service--detail-content-subTtl">説明文</h5>
                        <p class="desc">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col2-7--right">
                  <div class="thumb"><img class="cover border-radius-5" src="<?php echo $PATH;?>/assets/images/service/detail/detail04.png" alt=""></div>
                </div>
              </div>
            </div>
            <div class="p-service--detail-content-row">
              <h4 class="p-service--detail-content-ttl">トイレ清掃</h4>
              <div class="col2-7">
                <div class="col2-7--left">
                  <div class="col2-2">
                    <div class="col2-2-item">
                      <div class="col2-2-item--inner">
                        <h5 class="p-service--detail-content-subTtl">トイレ清掃(床の掃き)</h5>
                        <p class="desc">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                      </div>
                    </div>
                    <div class="col2-2-item">
                      <div class="col2-2-item--inner">
                        <h5 class="p-service--detail-content-subTtl">トイレ清掃(小便器洗浄)</h5>
                        <p class="desc">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                      </div>
                    </div>
                    <div class="col2-2-item">
                      <div class="col2-2-item--inner">
                        <h5 class="p-service--detail-content-subTtl">トイレ清掃(小便器拭き上げ)</h5>
                        <p class="desc">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                      </div>
                    </div>
                    <div class="col2-2-item">
                      <div class="col2-2-item--inner">
                        <h5 class="p-service--detail-content-subTtl">トイレ清掃(大便器洗浄)</h5>
                        <p class="desc">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                      </div>
                    </div>
                    <div class="col2-2-item">
                      <div class="col2-2-item--inner">
                        <h5 class="p-service--detail-content-subTtl">トイレ清掃(大便器拭き上げ)</h5>
                        <p class="desc">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                      </div>
                    </div>
                    <div class="col2-2-item">
                      <div class="col2-2-item--inner">
                        <h5 class="p-service--detail-content-subTtl">洗面台の洗浄</h5>
                        <p class="desc">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                      </div>
                    </div>
                    <div class="col2-2-item">
                      <div class="col2-2-item--inner">
                        <h5 class="p-service--detail-content-subTtl">洗面台の拭き上げ</h5>
                        <p class="desc">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                      </div>
                    </div>
                    <div class="col2-2-item">
                      <div class="col2-2-item--inner">
                        <h5 class="p-service--detail-content-subTtl">鏡の拭き上げ</h5>
                        <p class="desc">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                      </div>
                    </div>
                    <div class="col2-2-item">
                      <div class="col2-2-item--inner">
                        <h5 class="p-service--detail-content-subTtl">トイレ清掃(床の拭き)</h5>
                        <p class="desc">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                      </div>
                    </div>
                    <div class="col2-2-item">
                      <div class="col2-2-item--inner">
                        <h5 class="p-service--detail-content-subTtl">洗面台の洗浄</h5>
                        <p class="desc">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col2-7--right">
                  <div class="thumb"><img class="cover border-radius-5" src="<?php echo $PATH;?>/assets/images/service/detail/detail05.png" alt=""></div>
                </div>
              </div>
            </div>
            <div class="p-service--detail-content-row">
              <h4 class="p-service--detail-content-ttl">施設の清掃</h4>
              <div class="col2-7">
                <div class="col2-7--left">
                  <p class="desc">その他施設に設けられている箇所の清掃についての説明文が入ります。その他施設に設けられている箇所の清掃についての説明文が入ります。その他施設に設けられている箇所の清掃についての説明文が入ります。</p>
                </div>
                <div class="col2-7--right">
                  <div class="thumb"><img class="cover border-radius-5" src="<?php echo $PATH;?>/assets/images/service/detail/detail06.png" alt=""></div>
                </div>
              </div>
            </div>
          </div>
          <div class="p-service--detail-direct">
            <a href="" class="btn-blue">一覧に戻る</a>
          </div>
        </div>
      </div>
      <div class="p-service--nav">
        <div class="container">
          <div class="p-service--nav-ttl">
            <span class="en">SERVICE</span>
            <span class="jp">事業内容</span>
          </div>
          <div class="p-service--nav-list">
            <div class="p-service--nav-item">
              <a href="" class="p-service--nav-item-inner link border-radius-10">
                <div class="p-service--nav-item-infor">
                  <h3 class="p-service--nav-item-infor-ttl">建物総合管理事業</h3>
                </div>
                <div class="p-service--nav-item-icon">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-arrow-right-white.svg" alt="">
                </div>
                <img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/service/categories/service01.png" alt="">
              </a>
            </div>
            <div class="p-service--nav-item">
              <a href="" class="p-service--nav-item-inner link border-radius-10">
                <div class="p-service--nav-item-infor">
                  <h3 class="p-service--nav-item-infor-ttl">道路総合管理事業</h3>
                </div>
                <div class="p-service--nav-item-icon">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-arrow-right-white.svg" alt="">
                </div>
                <img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/service/categories/service02.png" alt="">
              </a>
            </div>
            <div class="p-service--nav-item">
              <a href="" class="p-service--nav-item-inner link border-radius-10">
                <div class="p-service--nav-item-infor">
                  <h3 class="p-service--nav-item-infor-ttl">設備修繕工事事業</h3>
                </div>
                <div class="p-service--nav-item-icon">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-arrow-right-white.svg" alt="">
                </div>
                <img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/service/categories/service03.png" alt="">
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>