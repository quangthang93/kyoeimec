<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <section class="p-end--mv">
    <h2 class="p-end--ttl">
      <span class="en">SERVICE</span>
      <span class="jp">事業内容</span>
    </h2>
  </section><!-- ./p-end--mv -->
  <div class="breadcrumbWrap">
    <div class="container">
      <div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li><a href="/">事業内容</a></li>
          <li>建物総合管理事業</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="p-end--cnt service">
    <div class="p-service">
      <div class="p-categories">
        <div class="container">
          <h3 class="p-categories--ttl">建物総合管理事業</h3>
          <p class="p-categories--desc desc">建物をお客様の資産と考え、トータルな管理・運営を行い建物の快適環境を守ります。</p>
          <div class="p-categories--list">
            <div class="p-categories--item">
              <div class="p-categories--item-inner">
                <div class="p-categories--item-thumb">
                  <img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/service/categories/cat01.png" alt="">
                </div>
                <div class="p-categories--item-info">
                  <h4 class="p-categories--item-ttl">日常清掃業務</h4>
                  <p class="desc2">施設の特徴を考慮し、清掃方法や効率的な作業シフトをご提案することで、施設の美観維持をお約束いたします。</p>
                </div>
              </div>
            </div>
            <div class="p-categories--item">
              <div class="p-categories--item-inner">
                <div class="p-categories--item-thumb">
                  <img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/service/categories/cat02.png" alt="">
                </div>
                <div class="p-categories--item-info">
                  <h4 class="p-categories--item-ttl">深夜清掃業務</h4>
                  <p class="desc2">深夜清掃に特化した専門チームが有ります。若手集団がパワフルにそして機敏に作業を行います。</p>
                </div>
              </div>
            </div>
            <div class="p-categories--item">
              <div class="p-categories--item-inner">
                <div class="p-categories--item-thumb">
                  <img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/service/categories/cat03.png" alt="">
                </div>
                <div class="p-categories--item-info">
                  <h4 class="p-categories--item-ttl">設備保守業務</h4>
                  <p class="desc2">建築物にかかわる様々な法令、ビル管理法、消防法、電機事業法、建築基準法等の遵守の為の忠実な業務を行います。</p>
                </div>
              </div>
            </div>
            <div class="p-categories--item">
              <div class="p-categories--item-inner">
                <div class="p-categories--item-thumb">
                  <img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/service/categories/cat04.png" alt="">
                </div>
                <div class="p-categories--item-info">
                  <h4 class="p-categories--item-ttl">駐車場業務</h4>
                  <p class="desc2">駐車場内の事故を防ぐ為の安全対策を施行し、安全走行の為の誘導や表示、場内巡回業務、入庫時の発券、出庫時の集金業務を行います。</p>
                </div>
              </div>
            </div>
            <div class="p-categories--item">
              <div class="p-categories--item-inner">
                <div class="p-categories--item-thumb">
                  <img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/service/categories/cat05.png" alt="">
                </div>
                <div class="p-categories--item-info">
                  <h4 class="p-categories--item-ttl">警備業務</h4>
                  <p class="desc2">施設人的警備、機械警備の双方からの管理を行うことで、管理体制を強化し施設の安全を守ります。</p>
                </div>
              </div>
            </div>
            <div class="p-categories--item">
              <div class="p-categories--item-inner">
                <div class="p-categories--item-thumb">
                  <img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/service/categories/cat06.png" alt="">
                </div>
                <div class="p-categories--item-info">
                  <h4 class="p-categories--item-ttl">受付業務</h4>
                  <p class="desc2">より良い施設運営の為に、様々な角度から施設の巡回や管理を行い、施設運営向上の為のご提案も行って参ります。</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="p-service--nav">
        <div class="container">
          <div class="p-service--nav-ttl">
            <span class="en">SERVICE</span>
            <span class="jp">事業内容</span>
          </div>
          <div class="p-service--nav-list">
            <div class="p-service--nav-item">
              <a href="" class="p-service--nav-item-inner link border-radius-10">
                <div class="p-service--nav-item-infor">
                  <h3 class="p-service--nav-item-infor-ttl">建物総合管理事業</h3>
                </div>
                <div class="p-service--nav-item-icon">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-arrow-right-white.svg" alt="">
                </div>
                <img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/service/categories/service01.png" alt="">
              </a>
            </div>
            <div class="p-service--nav-item">
              <a href="" class="p-service--nav-item-inner link border-radius-10">
                <div class="p-service--nav-item-infor">
                  <h3 class="p-service--nav-item-infor-ttl">道路総合管理事業</h3>
                </div>
                <div class="p-service--nav-item-icon">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-arrow-right-white.svg" alt="">
                </div>
                <img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/service/categories/service02.png" alt="">
              </a>
            </div>
            <div class="p-service--nav-item">
              <a href="" class="p-service--nav-item-inner link border-radius-10">
                <div class="p-service--nav-item-infor">
                  <h3 class="p-service--nav-item-infor-ttl">設備修繕工事事業</h3>
                </div>
                <div class="p-service--nav-item-icon">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-arrow-right-white.svg" alt="">
                </div>
                <img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/service/categories/service03.png" alt="">
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>