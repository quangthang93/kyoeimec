<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <section class="p-end--mv">
    <h2 class="p-end--ttl">
      <span class="en">SERVICE</span>
      <span class="jp">事業内容</span>
    </h2>
  </section><!-- ./p-end--mv -->
  <div class="breadcrumbWrap">
    <div class="container">
      <div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li>事業内容</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="p-end--cnt service">
      <div class="p-service">
        <section class="p-service--section">
          <div class="container">
            <div class="p-service--section-row">
              <a href="/service/categories/" class="p-service--section-row-headWrap link">
                <div class="p-service--section-row-head">
                  <h3 class="section-ttl2">
                    <span class="en white">BUILDING MANAGEMENT AND OPERATION</span>
                    <span class="jp white">建物総合管理事業</span>
                  </h3>
                  <div class="p-service--section-row-infor">
                    <p class="desc">建物をお客様の資産と考え、トータルな管理・運営を行い建物の快適環境を守ります。</p>
                  </div>
                </div>
                <div class="p-service--section-row-direct align-center">
                  <span class="view-more white">READ MORE</span>
                </div>
              </a>
            </div>
            <div class="p-service--section-row">
              <a href="/service/categories/" class="p-service--section-row-headWrap link type2">
                <div class="p-service--section-row-head">
                  <h3 class="section-ttl2">
                    <span class="en white">ROAD COMPREHENSIVE MANAGEMENT BUSINESS</span>
                    <span class="jp white">道路総合管理事業</span>
                  </h3>
                  <div class="p-service--section-row-infor">
                    <p class="desc">快適な環境への配慮を行い、地球環境の保全に貢献して安全で美しい道を守ります。</p>
                  </div>
                </div>
                <div class="p-service--section-row-direct align-center">
                  <span class="view-more white">READ MORE</span>
                </div>
              </a>
            </div>
            <div class="p-service--section-row">
              <a href="/service/categories/" class="p-service--section-row-headWrap link type3">
                <div class="p-service--section-row-head">
                  <h3 class="section-ttl2">
                    <span class="en white">EQUIPMENT CONSTRUCTION BUSINESS</span>
                    <span class="jp white">設備修繕工事事業</span>
                  </h3>
                  <div class="p-service--section-row-infor">
                    <p class="desc">快適な環境への配慮を行い、地球環境の保全に貢献して安全で美しい道を守ります。</p>
                  </div>
                </div>
                <div class="p-service--section-row-direct align-center">
                  <span class="view-more white">READ MORE</span>
                </div>
              </a>
            </div>
          </div>
        </section>
      </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>