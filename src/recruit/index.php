<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <section class="p-end--mv type3">
    <h2 class="p-end--ttl">
      <span class="en">RECRUIT</span>
      <span class="jp">採用情報</span>
    </h2>
  </section><!-- ./p-end--mv -->
  <div class="breadcrumbWrap type2">
    <div class="container">
      <div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li>採用情報</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="p-end--cnt type3">
    <div class="p-recruit">
      <div class="p-recruit--intro">
        <div class="container">
          <div class="p-recruit--intro-inner">
            <h3 class="p-recruit--intro-ttl">
              より良い街を、<br>暮らしを、未来を、<br>共につくろう
            </h3>
            <p class="desc">キョーエーメックは様々な事業を行っており、業務業務の幅が広いのが特徴です。それらはどれもまちをつくり、環境をつくり、人々の生活をつくります。より良い未来を、より確かな安心安全を私たちと一緒に作りましょう。</p>
          </div>
        </div>
      </div>
      <div class="p-recruit--challenge">
        <div class="container2">
          <div class="p-recruit--challenge-inner">
            <p class="p-recruit--challenge-ttl">
              <img src="<?php echo $PATH;?>/assets/images/recruit/ttl-challenge.png" alt="challenge">
            </p>
            <div class="p-recruit--challenge-cnt">
              <div class="p-recruit--challenge-cnt-item">
                <a href="" class="p-recruit--challenge-cnt-item-inner link border-radius-10">
                  <div class="p-recruit--challenge-cnt-item-infor">
                    <h3 class="p-recruit--challenge-cnt-item-infor-ttl">
                      <span class="jp">仕事を探す</span>
                      <span class="en">TOWN WORK</span>
                    </h3>
                  </div>
                  <div class="p-recruit--challenge-cnt-item-icon">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-arrow-right-white.svg" alt="">
                  </div>
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/recruit/challenge01.png" alt="">
                </a>
              </div>
              <div class="p-recruit--challenge-cnt-item">
                <a href="" class="p-recruit--challenge-cnt-item-inner link border-radius-10">
                  <div class="p-recruit--challenge-cnt-item-infor">
                    <h3 class="p-recruit--challenge-cnt-item-infor-ttl recruit">
                      <span class="jp">採用に関するお問い合わせ</span>
                      <span class="en">CONTACT</span>
                    </h3>
                  </div>
                  <div class="p-recruit--challenge-cnt-item-icon">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-arrow-right-white.svg" alt="">
                  </div>
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/recruit/challenge02.png" alt="">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="p-recruit--job">
        <div class="p-recruit--job-inner">
          <div class="container">
            <h2 class="section-ttl">
              <span class="en">JOB</span>
              <span class="jp">キョーエーメックの仕事</span>
            </h2>
            <div class="p-recruit--job-list">
              <div class="p-recruit--job-item">
                <div class="_cnt">
                  <p class="p-recruit--job-item-label">
                    <span class="txt en">JOB</span>
                    <span class="num en">01</span>
                  </p>
                  <h3 class="p-recruit--job-item-ttl">商業施設、複合施設の深夜清掃</h3>
                  <div class="p-recruit--job-item-likeBox">
                    <div class="_icon">
                      <img src="<?php echo $PATH;?>/assets/images/recruit/icon-like.svg" alt="">
                    </div>
                    <div class="_list">
                      <span class="_item">未経験歓迎</span>
                      <span class="_item">清掃</span>
                      <span class="_item">高収入</span>
                      <span class="_item">深夜作業</span>
                    </div>
                  </div>
                  <p class="desc">キョーエーメックの得意分野である清掃業務。閉店後の商業施設や複合施設を清掃します。 未経験の方にも経験豊富なスタッフが丁寧に指導します。 学生・フリーターなど若い方々を中心に色々な仲間が活躍中しています。</p>
                  <div class="p-recruit--job-item-suggest">
                    <p class="p-recruit--job-item-suggest-ttl">こんな方にオススメ！</p>
                    <ul class="p-recruit--job-item-suggest-list">
                      <li>安心して長く続けたい</li>
                      <li>作業中は黙々とこなし、集中して業務に取り組みたい</li>
                      <li>昼間の時間を自由に使いたい</li>
                      <li>未経験だけど0から学びたい</li>
                      <li>正社員を目指したい</li>
                    </ul>
                  </div>
                </div>
                <div class="_thumbWrap">
                  <div class="_thumb"><img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/recruit/job01.png" alt=""></div>
                </div>
              </div>
              <div class="p-recruit--job-item">
                <div class="_cnt">
                  <p class="p-recruit--job-item-label">
                    <span class="txt en">JOB</span>
                    <span class="num en">02</span>
                  </p>
                  <h3 class="p-recruit--job-item-ttl">道路保全のための清掃及び工事</h3>
                  <div class="p-recruit--job-item-likeBox">
                    <div class="_icon">
                      <img src="<?php echo $PATH;?>/assets/images/recruit/icon-like.svg" alt="">
                    </div>
                    <div class="_list">
                      <span class="_item">一般道路清掃</span>
                      <span class="_item">補修</span>
                      <span class="_item">高収入</span>
                      <span class="_item">深夜作業</span>
                    </div>
                  </div>
                  <p class="desc">道路全般の保全業務として歩道橋の清掃や整備、補修、違反広告物の除却、除雪、氷結対策の施行など、専門的技術を利かすことができる仕事です。責任感を持って務めることができます。安全で快適な道路にするため、私たちと一緒に働きましょう。</p>
                  <div class="p-recruit--job-item-suggest">
                    <p class="p-recruit--job-item-suggest-ttl">こんな方にオススメ！</p>
                    <ul class="p-recruit--job-item-suggest-list">
                      <li>安心して長く続けたい</li>
                      <li>作業中は黙々とこなし、集中して業務に取り組みたい</li>
                      <li>昼間の時間を自由に使いたい</li>
                      <li>未経験だけど0から学びたい</li>
                      <li>正社員を目指したい</li>
                    </ul>
                  </div>
                </div>
                <div class="_thumbWrap">
                  <div class="_thumb"><img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/recruit/job02.png" alt=""></div>
                </div>
              </div>
              <div class="p-recruit--job-item">
                <div class="_cnt">
                  <p class="p-recruit--job-item-label">
                    <span class="txt en">JOB</span>
                    <span class="num en">03</span>
                  </p>
                  <h3 class="p-recruit--job-item-ttl">ビル施設巡回点検・設備営繕工事</h3>
                  <div class="p-recruit--job-item-likeBox">
                    <div class="_icon">
                      <img src="<?php echo $PATH;?>/assets/images/recruit/icon-like.svg" alt="">
                    </div>
                    <div class="_list">
                      <span class="_item">未経験歓迎</span>
                      <span class="_item">ビル点検</span>
                      <span class="_item">高収入</span>
                      <span class="_item">深夜作業</span>
                    </div>
                  </div>
                  <p class="desc">担当する物件の電気・空調・給排水設備など日常(定期)点検や営繕工事、LED照明の取付工事、作業計画・管理報告書の作成といった設備関連の業務全般を行います。 ほか、物件のオーナー様、協力会社様との折衝・調節なども行い、業務に関わる様々な方とのコミュニケーションを大事にします。</p>
                  <div class="p-recruit--job-item-suggest">
                    <p class="p-recruit--job-item-suggest-ttl">こんな方にオススメ！</p>
                    <ul class="p-recruit--job-item-suggest-list">
                      <li>こんな方にオススメ！</li>
                      <li>細かい作業が得意な方</li>
                      <li>チームで仕事をするのが好きな方</li>
                      <li>コミュニケーションを積極的に取れる方</li>
                      <li>正社員を目指したい</li>
                    </ul>
                  </div>
                </div>
                <div class="_thumbWrap">
                  <div class="_thumb"><img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/recruit/job03.png" alt=""></div>
                </div>
              </div>
            </div>
          </div>
        </div>  
      </div>
      <div class="p-recruit--msg">
        <div class="container">
          <h3 class="p-recruit--msg-ttl">より良い街を、<br>暮らしを、未来を、<br>共につくろう</h3>
          <div class="p-recruit--msg-cnt">
            <div class="p-recruit--msg-cnt-item">
              <a href="" class="p-recruit--msg-cnt-item-inner link border-radius-10">
                <div class="p-recruit--msg-cnt-item-infor">
                  <h3 class="p-recruit--msg-cnt-item-infor-ttl">
                    <span class="jp">仕事を探す</span>
                    <span class="en">TOWN WORK</span>
                  </h3>
                </div>
                <div class="p-recruit--msg-cnt-item-icon">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-arrow-right-white.svg" alt="">
                </div>
              </a>
            </div>
            <div class="p-recruit--msg-cnt-item">
              <a href="" class="p-recruit--msg-cnt-item-inner link border-radius-10">
                <div class="p-recruit--msg-cnt-item-infor">
                  <h3 class="p-recruit--msg-cnt-item-infor-ttl recruit">
                    <span class="jp">採用に関するお問い合わせ</span>
                    <span class="en">CONTACT</span>
                  </h3>
                </div>
                <div class="p-recruit--msg-cnt-item-icon">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-arrow-right-white.svg" alt="">
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>