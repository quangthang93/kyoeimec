<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <section class="p-end--mv">
    <h2 class="p-end--ttl">
      <span class="en">PRIVACY POLICY</span>
      <span class="jp">プライバシーポリシー</span>
    </h2>
  </section><!-- ./p-end--mv -->
  <div class="breadcrumbWrap">
    <div class="container">
      <div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li>プライバシーポリシー</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="p-end--cnt">
    <div class="container">
      <div class="p-privacy">
        <h3 class="p-privacy--ttl">個人情報保護方針</h3>
        <p class="p-privacy--intro">当社は、都市の快適な環境の維持・向上を目指し、ビルや道路をはじめとする都市機能の総合メンテナンスに携わる会社として、個人情報保護の重要性を認識し、法令を遵守し、個人情報の適正な取扱いを推進していくことが、当社の社会的責務と考え、次の事項を含む個人情報保護方針を定め、これを実施し、かつ、維持することを宣言いたします。</p>
        <section class="p-privacy--list">
          <div class="p-privacy--item">
            <h4 class="p-privacy--item-ttl">個人情報とは</h4>
            <p class="p-privacy--item-desc">本プライバシーポリシーにおいて個人情報とは、株式会社キョーエーメック（以下キョーエーメックという）が運営／管理するウェブサイトのみならず、全ての事業を通じてお客様からご提供いただく氏名、住所、電話番号、電子メールアドレス等、お客様個人を識別できる情報あるいはお客様個人に固有の情報及び従業員等の個人情報を意味します。</p>
          </div>
          <div class="p-privacy--item">
            <h4 class="p-privacy--item-ttl">個人情報の利用目的</h4>
            <p class="p-privacy--item-desc">個人情報の取得、利用にあたっては、その利用目的を特定することとし、特定された利用目的の範囲内で利用します。個人情報をご本人の同意なく利用目的以外に利用することはありません。また、目的外利用を行わないために、適切な管理措置を講じます。</p>
          </div>
          <div class="p-privacy--item">
            <h4 class="p-privacy--item-ttl">個人情報の提供</h4>
            <p class="p-privacy--item-desc">ご本人の同意を得ている場合、法令に基づく場合等を除き、取得した個人情報を第三者に提供することはいたしません。</p>
          </div>
          <div class="p-privacy--item">
            <h4 class="p-privacy--item-ttl">関係法令およびその他の規範の遵守</h4>
            <p class="p-privacy--item-desc">個人情報の取扱いに関する法令、国が定める指針その他の規範を遵守します。</p>
          </div>
          <div class="p-privacy--item">
            <h4 class="p-privacy--item-ttl">個人情報の安全管理</h4>
            <p class="p-privacy--item-desc">当社は取得した個人情報を厳重に管理し、情報セキュリティ対策をはじめとする安全対策を実施し、個人情報への不正アクセス、漏洩、滅失又は毀損の防止及び是正に取り組みます。</p>
          </div>
          <div class="p-privacy--item">
            <h4 class="p-privacy--item-ttl">苦情及び相談</h4>
            <p class="p-privacy--item-desc">個人情報の取り扱いに関する苦情及び相談を受けた場合は、その内容について迅速に事実関係等を調査し、合理的な期間内に誠意をもって対応いたします。</p>
          </div>
          <div class="p-privacy--item">
            <h4 class="p-privacy--item-ttl">継続的改善</h4>
            <p class="p-privacy--item-desc">社会情勢・環境の変化を踏まえて、継続的に個人情報保護に関する個人情報保護マネジメントシステムを見直し、個人情報保護への取り組みを改善していきます。</p>
          </div>
        </section>
        <section class="p-privacy--notice">
          <h3 class="p-privacy--notice-ttl">プライバシーポリシー</h3>
          <p class="p-privacy--notice-intro">当社は、個人情報に関する法令を遵守し、個人情報の保護に万全を尽くため以下のように実施しております。</p>
          <div class="p-privacy--notice-list">
            <div class="p-privacy--notice-item">
              <h4 class="p-privacy--notice-item-ttl">お客さまの個人情報については、下記の目的の範囲内で適正に取り扱いさせていただきます。</h4>
              <ul class="list-dot">
                <li>お問合せ、ご相談にお答えすること</li>
                <li>ご本人確認、ご利用料金の請求、サービス提供条件の変更、停止・中止・契約解除の通知・その他当社サービスの提供に係ること</li>
                <li>電話、電子メール、郵送等各種媒体により、当社のサービスに関する販売促進等の情報提供を行うこと</li>
              </ul>
            </div>
            <div class="p-privacy--notice-item">
              <h4 class="p-privacy--notice-item-ttl">お客さまの個人情報を適正に取扱うため、個人情報への不正アクセスや個人情報の紛失、破壊、改ざんおよび漏洩等防止に関する適切な措置を行い、個人情報の保護に努めてまいります。ブラウザーからサーバーまでの情報伝達はSSLにて暗号化されて送信されます。</h4>
            </div>
            <div class="p-privacy--notice-item">
              <h4 class="p-privacy--notice-item-ttl">お客さまの個人情報について法令等に基づき裁判所・警察機関等の公的機関から開示の要請があった場合については、当該公的機関に提供することがございます。</h4>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>