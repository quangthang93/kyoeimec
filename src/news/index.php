<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <section class="p-end--mv">
    <h2 class="p-end--ttl">
      <span class="en">NEWS RELEASE</span>
      <span class="jp">お知らせ</span>
    </h2>
  </section><!-- ./p-end--mv -->
  <div class="breadcrumbWrap">
    <div class="container">
      <div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li>お知らせ</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="p-end--cnt">
    <div class="container">
      <div class="p-news">
        <ul class="p-news--list">
          <li class="p-news--item">
            <div class="p-news--item-dateWrap">
              <span class="date2">2021.10.12</span>
              <span class="tag type3">お知らせ</span>
            </div>
            <a href="/news/detail" class="p-news--item-ttl link">新型コロナウイルス感染拡大に伴う当社の対応について</a>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-dateWrap">
              <span class="date2">2021.10.12</span>
              <span class="tag type3">SDGs</span>
            </div>
            <a href="/news/detail" class="p-news--item-ttl link">[横浜型地域貢献企業]認定書を更新掲載しました</a>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-dateWrap">
              <span class="date2">2021.10.12</span>
              <span class="tag type3">グループ会社</span>
            </div>
            <a href="/news/detail" class="p-news--item-ttl link">ISO9001・ISO140001の更新審査が完了致しました</a>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-dateWrap">
              <span class="date2">2021.10.12</span>
              <span class="tag type3">グループ会社</span>
            </div>
            <a href="/news/detail" class="p-news--item-ttl link-icon blank">外部リンクの場合はこのような表示になります</a>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-dateWrap">
              <span class="date2">2021.10.12</span>
              <span class="tag type3">SDGs</span>
            </div>
            <a href="/news/detail" class="p-news--item-ttl link-icon pdf">PDFリンクの場合はこのような表示になります</a>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-dateWrap">
              <span class="date2">2021.10.12</span>
              <span class="tag type3">グループ会社</span>
            </div>
            <a href="/news/detail" class="p-news--item-ttl link">新着情報のタイトルがこちらに掲載されます。記事を更新すると自動で取得します。</a>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-dateWrap">
              <span class="date2">2021.10.12</span>
              <span class="tag type3">SDGs</span>
            </div>
            <a href="/news/detail" class="p-news--item-ttl link">新着情報のタイトルがこちらに掲載されます。記事を更新すると自動で取得します。</a>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-dateWrap">
              <span class="date2">2021.10.12</span>
              <span class="tag type3">SDGs</span>
            </div>
            <a href="/news/detail" class="p-news--item-ttl link">新着情報のタイトルがこちらに掲載されます。記事を更新すると自動で取得します。</a>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-dateWrap">
              <span class="date2">2021.10.12</span>
              <span class="tag type3">SDGs</span>
            </div>
            <a href="/news/detail" class="p-news--item-ttl link">新着情報のタイトルがこちらに掲載されます。記事を更新すると自動で取得します。</a>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-dateWrap">
              <span class="date2">2021.10.12</span>
              <span class="tag type3">SDGs</span>
            </div>
            <a href="/news/detail" class="p-news--item-ttl link">新着情報のタイトルがこちらに掲載されます。記事を更新すると自動で取得します。</a>
          </li>
        </ul>
      </div>
      <div class="pagination">
        <div class="pagination-list">
          <a class="ctrl prev" href="">
            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-left" class="svg-inline--fa fa-arrow-left fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="#1F8CFA" d="M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z"></path></svg>
          </a>
          <a class="active" href="">1</a>
          <a href="">2</a>
          <a href="">3</a>
          <a href="">4</a>
          <a href="">5</a>
          <a href="">6</a>
          <div class="pagination-spacer">…</div>
          <a href="">12</a>
          <a class="ctrl next" href="">
            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-right" class="svg-inline--fa fa-arrow-right fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="#1F8CFA" d="M190.5 66.9l22.2-22.2c9.4-9.4 24.6-9.4 33.9 0L441 239c9.4 9.4 9.4 24.6 0 33.9L246.6 467.3c-9.4 9.4-24.6 9.4-33.9 0l-22.2-22.2c-9.5-9.5-9.3-25 .4-34.3L311.4 296H24c-13.3 0-24-10.7-24-24v-32c0-13.3 10.7-24 24-24h287.4L190.9 101.2c-9.8-9.3-10-24.8-.4-34.3z"></path></svg>
          </a>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>