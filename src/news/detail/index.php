<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <section class="p-end--mv">
    <h2 class="p-end--ttl">
      <span class="en">NEWS RELEASE</span>
      <span class="jp">お知らせ</span>
    </h2>
  </section><!-- ./p-end--mv -->
  <div class="breadcrumbWrap">
    <div class="container">
      <div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li><a href="/news">お知らせ</a></li>
          <li>会社情報の役員が変更となりました</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="p-end--cnt">
    <div class="container">
      <div class="p-news">
        <div class="p-news--detail">
          <div class="p-news--detail-head">
            <span class="tag type3">お知らせ</span>
          </div>
          <div class="p-news--detail-cnt">
            <h1 class="p-news--detail-ttl">会社情報の役員が変更となりました</h1>
            <div class="p-news--detail-dateWrap">
              <span class="date2">2021.10.12</span>
            </div>
            <p class="p-news--detail-desc">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
            <div class="no-reset">
              <div class="align-center mgb-60">
                <img src="<?php echo $PATH;?>/assets/images/news/new01.png" alt="">
              </div>
              <div class="mgb-40">
                <h2 class="mgb-20">中見出しが入ります</h2>
                <p>
                  <b>この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</b>
                </p>
              </div>
              <div class="mgb-40">
                <h3 class="mgb-20">小見出しが入ります</h3>
                <p>
                  この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。
                </p>
              </div>
              <div class="mgb-50">
                <div class="mgb-15">
                  <a class="link-icon under blank" href="">テキストリンク</a>
                </div>
                <div class="mgb-15">
                  <a class="link-icon under pdf" href="">テキストリンク</a>
                </div>
              </div>
              <div class="mgb-100">
                <div class="col2">
                  <div class="col2-item">
                    <h2 class="mgb-20">中見出しが入ります</h2>
                    <p>
                      この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。
                    </p>
                  </div>
                  <div class="col2-item">
                    <img src="<?php echo $PATH;?>/assets/images/news/new02.png" alt="">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="p-news--detail-direct">
            <a href="/news" class="btn-back">一覧に戻る</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>