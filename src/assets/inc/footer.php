    <section class="section-contact">
      <div class="section-contact--inner">
        <h2 class="section-contact--ttl">お気軽にお問い合わせください。</h2>
        <p class="section-contact--desc">キョーエーメックへのお仕事のご依頼、サービスに関するご質問などはこちらから受け付けております。<br class="pc-only"> どんな些細なことでも構いません、お気軽にお問い合わせください。</p>
        <div class="section-contact--direct">
          <a href="/contact" class="btn-contact">お問い合わせ</a>
        </div>
      </div>
      <div class="pagetop js-pageTop link">PAGETOP</div>
    </section>

    <footer class="footer">
      <div class="footer-inner">
        <div class="footer-top">
          <div class="footer-infor">
            <div class="footer-logo">
              <a class="link" href="/"><img src="<?php echo $PATH;?>/assets/images/common/h-logo.png" alt=""></a>
            </div><!-- ./footer-logo -->
            <div class="footer-infor--cnt">
              〒231-0023 神奈川県横浜市中区山下町2番地 <br>産業貿易センタービル <br>TEL:045-681-5470
            </div>
          </div><!-- ./footer-top -->
          <div class="footer-nav">
            <div class="footer-nav--col">
              <ul>
                <li><a class="link" href="">私たちについて</a></li>
              </ul>
            </div>
            <div class="footer-nav--col">
              <ul>
                <li><a class="link" href="">事業内容</a></li>
                <li><a class="type2 link" href="">建物総合管理事業</a></li>
                <li><a class="type2 link" href="">道路総合管理事業</a></li>
                <li><a class="type2 link" href="">設備修繕工事事業</a></li>
              </ul>
            </div>

            <div class="footer-nav--col">
              <ul>
                <li><a class="link" href="">SDGsへの取り組み</a></li>
              </ul>
            </div>
            <div class="footer-nav--col">
              <ul>
                <li><a class="link" href="">会社案内</a></li>
                <li><a class="type2 link" href="">会社概要</a></li>
                <li><a class="type2 link" href="">CSR基本方針</a></li>
                <li><a class="type2 link" href="">認証取得</a></li>
              </ul>              
            </div>
            <div class="footer-nav--col">
              <ul>
                <li><a class="link" href="">お知らせ</a></li>
                <li><a class="link" href="">お問い合わせ</a></li>
                <li><a class="link" href="">個人情報保護方針</a></li>
              </ul>
            </div>
          </div><!-- ./footer-nav -->
        </div>
        
        <div class="footer-copyWrap">
          <div class="footer-partner">
            <ul>
              <li>
                <a class="link" href="">
                  <img src="<?php echo $PATH;?>/assets/images/common/partner01.png" alt="">
                </a>
              </li>
              <li>
                <a class="link" href="">
                  <img src="<?php echo $PATH;?>/assets/images/common/partner02.png" alt="">
                </a>
              </li>
              <li>
                <a class="link" href="">
                  <img src="<?php echo $PATH;?>/assets/images/common/partner03.png" alt="">
                </a>
              </li>
            </ul>
          </div>
          <p class="footer-copy">Copyright © KYOEI MEC All rights reserved.</p>
        </div><!-- ./footer-copy -->
      </div><!-- ./footer-inner -->
    </footer><!-- ./footer -->
  </div>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery-3.5.1.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/wow.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/ofi.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/slick.min.js"></script>
  <!-- <script src="<?php echo $PATH;?>/assets/js/libs/jquery-ui.min.js"></script> -->
  <!-- <script src="<?php echo $PATH;?>/assets/js/libs/datepicker-ja.js"></script> -->
  <!-- <script src="<?php echo $PATH;?>/assets/js/libs/remodal.min.js"></script> -->
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery.matchHeight-min.js"></script>
  <!-- <script src="<?php echo $PATH;?>/assets/js/libs/gsap.min.js"></script> -->
  <script src="<?php echo $PATH;?>/assets/js/common.js"></script>
  <!-- <script src="<?php echo $PATH;?>/assets/js/top.js"></script> -->
</body>

</html>