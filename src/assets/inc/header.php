<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/variables.php'; ?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Title</title>
  <meta name="description" content="">
  <meta property="og:site_name" content="">
  <meta property="og:title" content="">
  <meta property="og:description" content="">
  <meta property="og:type" content="website">
  <meta property="og:url" content="">
  <meta property="og:image" content="">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="">
  <meta name="twitter:image" content="">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@100;300;400;500;600;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo $PATH;?>/assets/css/index.css">
</head>

<body>
  <div class="wrapper">
    <header class="header js-header wow fadeIn">
      <h1 class="header-logo">
        <a href="/">
          <img class="js-logo-pc h-logo-pc" src="<?php echo $PATH;?>/assets/images/common/h-logo.png" alt="">
          <img class="js-logo-sp h-logo-sp" src="<?php echo $PATH;?>/assets/images/common/h-logo-white.png" alt="">
        </a>
      </h1>
      <div class="sp-only sp-navBtn js-sp-navBtn">
        <span></span>
        <span></span>
      </div>
      <nav class="menu js-menu">
        <div class="menu-inner">
          <ul class="menu-list">
            <li class="menu-item">
              <a href="/about"><span class="en sp-only">ABOUT</span><span>私たちについて</span></a>
            </li>
            <li class="menu-item">
              <a href="/service"><span class="en sp-only">SERVICE</span><span>事業内容</span></a>
            </li>
            <!-- <li class="menu-item">
              <a href="/case"><span class="en sp-only">CASE</span><span>実績紹介</span></a>
            </li> -->
            <li class="menu-item">
              <a href="/sustainability"><span class="en sp-only">SUSTAINABILITY</span><span>SDGsへの取り組み</span></a>
            </li>
            <li class="menu-item">
              <a href="/company"><span class="en sp-only">COMPANY</span><span>会社案内</span></a>
            </li>
            <li class="menu-item">
              <a href="/recruit"><span class="en sp-only">RECRUIT</span><span>採用情報</span></a>
            </li>
            <li class="menu-item">
              <a href="/news"><span class="en sp-only">NEWS RELEASE</span><span>お知らせ</span></a>
            </li>
            <li class="menu-item sp-only">
              <a href="/contact"><span class="en sp-only">CONTACT</span><span>お問い合わせ</span></a>
            </li>
          </ul>
          <p class="menu-item contact pc-only">
            <a href="/contact" class="btn-contact">
              お問い合わせ
            </a>
          </p>
        </div>
      </nav><!-- ./menu -->
    </header><!-- ./header -->