<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <section class="p-end--mv">
    <h2 class="p-end--ttl">
      <span class="en">COMPANY</span>
      <span class="jp">会社案内</span>
    </h2>
  </section><!-- ./p-end--mv -->
  <div class="breadcrumbWrap">
    <div class="container">
      <div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li><a href="/company">会社案内</a></li>
          <li>CSR基本方針</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="p-end--cnt">
    <div class="container">
      <div class="p-company">
        <h3 class="p-end--ttl2">CSR基本方針</h3>
        <p class="p-company-desc desc3">当社は、すべてのステークホルダーの皆様に 信頼される企業を目指します。</p>
        <div class="p-csr">
          <h4 class="p-end--subTtl">法令順守宣誓</h4>
          <p class="desc">株式会社キョーエーメックは、全ての事業活動において全社員が適用される法令を現在及び将来にわたって順守し、健全で透明性のある事業活動を行うことを宣誓いたします。</p>
          <div class="p-csr--cnt">
            <h4 class="p-end--subTtl">地域志向CSR方針</h4>
            <ul class="p-csr--list">
              <li>
                <h5 class="ttl">1.コンプライアンス</h5>
                <p class="desc">事業に関わる全ての法律、条令を把握・遵守するとともに、コンプライアンス関連諸規定に従い、全社でのコンプライアンス体制を推進します。</p>
              </li>
              <li>
                <h5 class="ttl">2.お客様へ</h5>
                <p class="desc">法令遵守・リスク管理を心がけ、創意を凝らし、顧客や取引先の要望に最大限応える知識と技術の向上を常に目指します。</p>
              </li>
              <li>
                <h5 class="ttl">3.従業員へ</h5>
                <p class="desc">社員一人ひとりがお互いのパートナーとして尊重しあい、社員一人ひとりの能力を最大限に発揮できるよう安心で快適な職場環境を目指します。</p>
              </li>
              <li>
                <h5 class="ttl">4.地域の皆様へ</h5>
                <p class="desc">地域の一員として地域や社会との関わりを大切にし、環境保全に努め、地域社会の発展に貢献します。</p>
              </li>
              <li>
                <h5 class="ttl">5.取引先へ</h5>
                <p class="desc">取引先各社と良好な関係を維持し、公正で誠実な取引を行い、共に発展することを目指します。</p>
              </li>
              <li>
                <h5 class="ttl">6.環境への配慮</h5>
                <p class="desc">ビルメンテナンス事業・道路メンテナンス事業・環境事業を通して、環境保全活動に貢献します。 (リサイクル・ゴミ分別・ＣＯ2削減等)</p>
              </li>
              <li>
                <h5 class="ttl">7.環境への配慮</h5>
                <p class="desc">ビルメンテナンス事業・道路メンテナンス事業・環境事業を通して、環境保全活動に貢献します。 (リサイクル・ゴミ分別・ＣＯ2削減等)</p>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="p-company--direct">
    <div class="container">
      <ul class="p-company--direct-list">
        <li><a href="/company/profile">会社概要</a></li>
        <li><a href="/company/csr">CSR基本方針</a></li>
        <li><a href="/company/certification">認証取得</a></li>
      </ul>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>