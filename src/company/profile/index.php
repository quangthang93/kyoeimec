<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <section class="p-end--mv">
    <h2 class="p-end--ttl">
      <span class="en">COMPANY</span>
      <span class="jp">会社案内</span>
    </h2>
  </section><!-- ./p-end--mv -->
  <div class="breadcrumbWrap">
    <div class="container">
      <div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li><a href="/company">会社案内</a></li>
          <li>会社概要</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="p-end--cnt">
    <div class="container">
      <div class="p-company">
        <h3 class="p-end--ttl2">会社概要</h3>
        <div class="p-profile">
          <div class="p-profile--cnt">
            <section class="p-profile--head" id="philosophy">
              <h4 class="p-end--subTtl">経営理念</h4>
              <div class="p-profile--head-list">
                <p class="p-profile--head-item">創 意</p>
                <p class="p-profile--head-item">情 熱</p>
                <p class="p-profile--head-item">信 義</p>
              </div>
            </section>
            <section class="p-profile--row" id="policy">
              <h4 class="p-end--subTtl">経営方針</h4>
              <p class="desc4">情熱を持った人材と卓越した技術と発想で高い付加価値を創造し、お客様の満足度の向上と社会・環境への貢献をはかってまいります。</p>
            </section>
            <section class="p-profile--row" id="guidelines">
              <h4 class="p-end--subTtl">経営方針</h4>
              <ul class="line-list">
                <li>私たちは「<span class="txt-blue">責任</span>」を持った行動をします </li>
                <li>私たちは「<span class="txt-blue">誠実</span>」に行動をします</li>
                <li>私たちは「<span class="txt-blue">迅速</span>」に行動をします</li>
                <li>私たちは「<span class="txt-blue">活発</span>」に行動をします</li>
                <li>私たちは「<span class="txt-blue">柔軟</span>」な発想で行動します</li>
              </ul>
            </section>
            <section class="p-profile--row" id="about">
              <h4 class="p-end--subTtl">会社情報</h4>
              <div class="p-profile--table">
                <div class="table">
                  <div class="table-row">
                    <div class="table-th">社名</div>
                    <div class="table-td">株式会社キョーエーメック</div>
                  </div>
                  <div class="table-row">
                    <div class="table-th">本社所在地</div>
                    <div class="table-td">〒231-0023<br> 神奈川県横浜市中区山下町2番地 産業貿易センタービル</div>
                  </div>
                  <div class="table-row">
                    <div class="table-th">電話番号</div>
                    <div class="table-td">代表: 045-681-5470</div>
                  </div>
                  <div class="table-row">
                    <div class="table-th">FAX番号</div>
                    <div class="table-td">045-681-5471</div>
                  </div>
                  <div class="table-row">
                    <div class="table-th">役員構成</div>
                    <div class="table-td">
                      <div class="subTable">
                        <div class="subTable-item">
                          <div class="ttl type1">代表取締役社長</div>
                          <div class="cnt">山口拓郎</div>
                        </div>
                        <div class="subTable-item">
                          <div class="ttl type1">常務取締役</div>
                          <div class="cnt">石田一幸</div>
                        </div>
                        <div class="subTable-item">
                          <div class="ttl type1">取締役</div>
                          <div class="cnt">河﨑克彦</div>
                        </div>
                        <div class="subTable-item">
                          <div class="ttl type1">取締役</div>
                          <div class="cnt">高本豊秀</div>
                        </div>
                        <div class="subTable-item">
                          <div class="ttl type1">取締役(非常勤)</div>
                          <div class="cnt">橋本圭史</div>
                        </div>
                        <div class="subTable-item">
                          <div class="ttl type1">取締役(非常勤)</div>
                          <div class="cnt">入澤秀史</div>
                        </div>
                        <div class="subTable-item">
                          <div class="ttl type1">監査役</div>
                          <div class="cnt">山口達也</div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="table-row">
                    <div class="table-th">設立</div>
                    <div class="table-td">平成14年3月</div>
                  </div>
                  <div class="table-row">
                    <div class="table-th">従業員数</div>
                    <div class="table-td">338名(2021年4月1日現在)</div>
                  </div>
                  <div class="table-row">
                    <div class="table-th">資本金</div>
                    <div class="table-td">4000万円</div>
                  </div>
                  <div class="table-row">
                    <div class="table-th">保有資格</div>
                    <div class="table-td">
                      <div class="subTable2">
                        <div class="subTable2-item">
                          建築物環境衛生管理技術者<br>
                          電気主任技術者<br>
                          電気工事士<br>
                          危険物取扱者<br>
                          消防設備点検資格者<br>
                          消防設備士<br>
                          ボイラー技師<br>
                          ボイラー整備士<br>
                          ボイラー取扱作業主任者<br>
                          清掃作業監督者<br>
                          ビルクリーニング技能士
                        </div>
                        <div class="subTable2-item">
                          空気環境測定実施者<br>
                          貯水槽清掃作業従事者<br>
                          防除作業監督者<br>
                          警備指導教育責任者<br>
                          機械警備業務管理者<br>
                          衛生管理者<br>
                          宅地建物取引主任者<br>
                          エネルギー管理士<br>
                          エネルギー管理員
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="table-row">
                    <div class="table-th">所属団体</div>
                    <div class="table-td">社団法人神奈川県ビルメンテナンス協会 <br>
                    社団法人 神奈川県警備業協会 <br>
                  横浜道路清掃事業協同組合</div>
                  </div>
                  <div class="table-row">
                    <div class="table-th">登録免許等</div>
                    <div class="table-td">一般建設業(電気工事業・管工事業・消防施設工事業) 神奈川県知事許可 (般一29) 第84736号<br>電気工事業(一般電気工作物及び自家用電気工作物)神奈川県知事届出 第300021号<br>建築物環境衛生総合管理業<br>建築物飲料水貯水槽清掃業<br>建築物ねずみ昆虫等防除業<br>警備業<br>産業廃棄物収集運搬業<br>一般廃棄物収集運搬業<br> ISO9001<br>ISO14001など</div>
                  </div>
                </div>
              </div>
            </section>
            <section class="p-profile--row" id="access">
              <h4 class="p-end--subTtl">交通アクセス</h4>
              <div class="p-profile--access">
                <div class="p-profile--access-map">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8057.580450018287!2d139.64382754724488!3d35.44665670091737!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60185cfc87bfffff%3A0x837f2cb275bbb3d!2z55Sj5qWt6LK_5piT44K744Oz44K_44O844OT44Or!5e0!3m2!1sja!2sjp!4v1638513114100!5m2!1sja!2sjp" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
                <div class="p-profile--access-guide desc">みなとみらい線日本大通り駅から徒歩4分 <br>みなとみらい線元町・中華街駅から徒歩6分</div>
              </div>
            </section>
            <section class="p-profile--row" id="history">
              <h4 class="p-end--subTtl">沿革</h4>
              <div class="table">
                <div class="table-row">
                  <div class="table-th type2">
                    1965
                  </div>
                  <div class="table-td type2">
                    <div class="_info">
                      <div class="numWrap">
                        <span class="num">6</span>
                        <span class="unit">月</span>
                      </div>
                      <p class="_cnt">株式会社横浜協栄社設立</p>
                    </div>
                  </div>
                </div>
                <div class="table-row">
                  <div class="table-th type2">
                    1988
                  </div>
                  <div class="table-td type2">
                    <div class="_info">
                      <div class="numWrap">
                        <span class="num">6</span>
                        <span class="unit">月</span>
                      </div>
                      <p class="_cnt">キョーエーメック株式会社に称号変更</p>
                    </div>
                  </div>
                </div>
                <div class="table-row">
                  <div class="table-th type2">
                    2002
                  </div>
                  <div class="table-td type2">
                    <div class="_info">
                      <div class="numWrap">
                        <span class="num">3</span>
                        <span class="unit">月</span>
                      </div>
                      <p class="_cnt">株式会社キョーエーヨコハマ設立<br> 資本金3000万円</p>
                    </div>
                  </div>
                </div>
                <div class="table-row">
                  <div class="table-th type2">
                    2002
                  </div>
                  <div class="table-td type2">
                    <div class="_info">
                      <div class="numWrap">
                        <span class="num">9</span>
                        <span class="unit">月</span>
                      </div>
                      <p class="_cnt">キョーエーメック株式会社(現: 株式会社ケイミックス)より、会社分割によって事業継承を受け営業開始 資本金3500万円に増資</p>
                    </div>
                  </div>
                </div>
                <div class="table-row">
                  <div class="table-th type2">
                    2003
                  </div>
                  <div class="table-td type2">
                    <div class="_info">
                      <div class="numWrap">
                        <span class="num">9</span>
                        <span class="unit">月</span>
                      </div>
                      <p class="_cnt">資本金4500万円に増資</p>
                    </div>
                  </div>
                </div>
                <div class="table-row">
                  <div class="table-th type2">
                    2005
                  </div>
                  <div class="table-td type2">
                    <div class="_info">
                      <div class="numWrap">
                        <span class="num">5</span>
                        <span class="unit">月</span>
                      </div>
                      <p class="_cnt">本社を現在の横浜市中区山下町2番地 産業貿易センタービルに移転</p>
                    </div>
                  </div>
                </div>
                <div class="table-row">
                  <div class="table-th type2">
                    2007
                  </div>
                  <div class="table-td type2">
                    <div class="_info">
                      <div class="numWrap">
                        <span class="num">1</span>
                        <span class="unit">月</span>
                      </div>
                      <p class="_cnt">ISO9001 ISO14001認証取得</p>
                    </div>
                  </div>
                </div>
                <div class="table-row">
                  <div class="table-th type2">
                    2010
                  </div>
                  <div class="table-td type2">
                    <div class="_info">
                      <div class="numWrap">
                        <span class="num">11</span>
                        <span class="unit">月</span>
                      </div>
                      <p class="_cnt">株式会社メックコミュニティを吸収合併<br> 資本金5500万円に増資</p>
                    </div>
                  </div>
                </div>
                <div class="table-row">
                  <div class="table-th type2">
                    2011
                  </div>
                  <div class="table-td type2">
                    <div class="_info">
                      <div class="numWrap">
                        <span class="num">6</span>
                        <span class="unit">月</span>
                      </div>
                      <p class="_cnt">株式会社キョーエーメックに称号変更</p>
                    </div>
                    <div class="_info">
                      <div class="numWrap">
                        <span class="num">8</span>
                        <span class="unit">月</span>
                      </div>
                      <p class="_cnt">資本金4000万円に減資</p>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
          <div class="p-profile--nav">
            <p class="p-profile--nav-ttl">会社概要</p>
            <ul class="p-profile--nav-list">
              <li><a href="#philosophy">経営理念</a></li>
              <li><a href="#policy">経営方針</a></li>
              <li><a href="#guidelines">行動指針</a></li>
              <li><a href="#about">会社情報</a></li>
              <li><a href="#access">交通アクセス</a></li>
              <li><a href="#history">沿革</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="p-company--direct">
    <div class="container">
      <ul class="p-company--direct-list">
        <li><a href="/company/profile">会社概要</a></li>
        <li><a href="/company/csr">CSR基本方針</a></li>
        <li><a href="/company/certification">認証取得</a></li>
      </ul>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>