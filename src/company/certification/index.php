<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <section class="p-end--mv">
    <h2 class="p-end--ttl">
      <span class="en">COMPANY</span>
      <span class="jp">会社案内</span>
    </h2>
  </section><!-- ./p-end--mv -->
  <div class="breadcrumbWrap">
    <div class="container">
      <div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li><a href="/company">会社案内</a></li>
          <li>認証取得</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="p-end--cnt">
    <div class="container">
      <div class="p-company">
        <h3 class="p-end--ttl2">認証取得</h3>
        <p class="p-company-desc desc3">当社では両規格を全業種で認証取得し、 設備業務、清掃業務、駐車場管理、警備業務、道路清掃、道路保全 それぞれの業種がマニュアルにそった業務を行い、 品質を維持し環境に良い確実な業務遂行を行っています。 このことは、お客様への安心、安全、そして信頼いただける確実な業務を行うことを お約束できる証です。</p>
        <div class="p-certification">
          <h4 class="p-end--subTtl">認証・許可</h4>
          <p class="desc">品質及び環境の両規格で要求されているのは継続的な改善です。システムをより良いものに改善する為に、PDCAサイクルに基づき業務を行います。顧客満足、品質の向上、環境の保全、これらを常に意識した継続的改善を行い、よりよいサービスを提供できるシステム運用を行ってまいります。</p>
          <div class="p-certification--cnt">
            <ul class="p-certification--list">
              <li>品質マネジメントシステム(ISO 9001)</li>
              <li>横浜型地域貢献企業 最上位認定</li>
              <li>環境マネジメントシステム(ISO 14001)</li>
              <li>特定建設業 国土交通大臣許可(特-2)第4679号</li>
              <li>電気工事業許可</li>
              <li>一般建設業 国土交通大臣許可(般-2)第4679号</li>
              <li>一般建設業許可</li>
              <li>一般労働者派遣業 許可番号 派13-303369</li>
            </ul>
            <ul class="p-certification--listImg">
              <li>
                <div class="inner">
                  <div class="thumb">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/company/certification01.png" alt="">
                  </div>
                  <p class="ttl">このテキストはキャプションです。</p>
                </div>
              </li>
              <li>
                <div class="inner">
                  <div class="thumb">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/company/certification02.png" alt="">
                  </div>
                  <p class="ttl">このテキストはキャプションです。</p>
                </div>
              </li>
              <li>
                <div class="inner">
                  <div class="thumb">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/company/certification03.png" alt="">
                  </div>
                  <p class="ttl">このテキストはキャプションです。</p>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="p-company--direct">
    <div class="container">
      <ul class="p-company--direct-list">
        <li><a href="/company/profile">会社概要</a></li>
        <li><a href="/company/csr">CSR基本方針</a></li>
        <li><a href="/company/certification">認証取得</a></li>
      </ul>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>