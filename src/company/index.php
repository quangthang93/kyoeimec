<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <section class="p-end--mv">
    <h2 class="p-end--ttl">
      <span class="en">COMPANY</span>
      <span class="jp">会社案内</span>
    </h2>
  </section><!-- ./p-end--mv -->
  <div class="breadcrumbWrap">
    <div class="container">
      <div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li>会社案内</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="p-end--cnt">
    <div class="container">
      <div class="p-company">
        <p class="p-company--intro">情熱を持った人材と卓越した技術と発想で高い付加価値を創造し、お客様の満足度の向上と社会・環境への貢献をはかってまいります。</p>
        <div class="p-company--list">
          <div class="p-company--list-item">
            <a href="/company/profile" class="p-company--list-item-inner link border-radius-10">
              <div class="p-company--list-item-infor">
                <h3 class="p-company--list-item-infor-ttl">
                  <span class="jp">会社概要</span>
                </h3>
              </div>
              <div class="p-company--list-item-icon">
                <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-arrow-right-white.svg" alt="">
              </div>
              <img class="cover" src="<?php echo $PATH;?>/assets/images/company/company01.png" alt="">
            </a>
          </div>
          <div class="p-company--list-item">
            <a href="/company/csr" class="p-company--list-item-inner link border-radius-10">
              <div class="p-company--list-item-infor">
                <h3 class="p-company--list-item-infor-ttl">
                  <span class="jp">CSR基本方針</span>
                </h3>
              </div>
              <div class="p-company--list-item-icon">
                <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-arrow-right-white.svg" alt="">
              </div>
              <img class="cover" src="<?php echo $PATH;?>/assets/images/company/company02.png" alt="">
            </a>
          </div>
          <div class="p-company--list-item">
            <a href="/company/certification" class="p-company--list-item-inner link border-radius-10">
              <div class="p-company--list-item-infor">
                <h3 class="p-company--list-item-infor-ttl">
                  <span class="jp">認証取得</span>
                </h3>
              </div>
              <div class="p-company--list-item-icon">
                <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-arrow-right-white.svg" alt="">
              </div>
              <img class="cover" src="<?php echo $PATH;?>/assets/images/company/company03.png" alt="">
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>