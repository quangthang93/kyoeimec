<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <section class="section-mv wow fadeIn">
    <h2 class="section-mv--ttl wow fadeInUp">
      <img class="" src="<?php echo $PATH;?>/assets/images/top/mv-ttl.svg" alt="">
    </h2>
    <div class="section-mv--image sp-only">
    	<img class="" src="<?php echo $PATH;?>/assets/images/top/mv.png" alt="">
    </div>
    <div class="section-mv--mask pc-only">
    </div>
    <div class="section-mv--video pc-only">
      <video autoplay muted loop>
        <source src="<?php echo $PATH;?>/assets/video/mv-video.mp4" type="video/mp4">
        <source src="<?php echo $PATH;?>/assets/video/mv-video.mp4" type="video/ogg">
      </video>
    </div>
  </section><!-- ./section-mv -->
  <section class="section-about">
    <div class="container2">
      <div class="section-about--inner">
        <div class="section-about--ttlWrap wow fadeInLeft">
          <h2 class="section-ttl">
            <span class="en">ABOUT</span>
            <span class="jp">私たちについて</span>
          </h2>
          <a href="" class="btn-blue pc-only">私たちについて</a>
        </div>
        <div class="section-about--cnt wow fadeInRight">
          <h3 class="section-about--cnt-ttl">移り行く時代のニーズや社会に適した事業のアップデートより良い未来を創造します</h3>
          <p class="desc">社会、自然、環境、街、人、全てが大切なことだから、 キョーエーメックはより良い未来を創造するため、日々邁進していきます。</p>
          <a href="" class="btn-blue sp-only">私たちについて</a>
        </div>
      </div>
    </div>
  </section><!-- ./section-about -->
  <section class="section-service">
    <div class="section-service--inner">
      <div class="container2">
        <h2 class="section-ttl wow fadeInUp">
          <span class="en">SERVICE</span>
          <span class="jp">事業内容</span>
        </h2>
        <div class="section-service--cnt">
          <div class="section-service--cnt-pickup">
            <div class="section-service--cnt-pickup-ttlWrap wow fadeInLeft">
              <h3 class="section-ttl2">
                <span class="en">BUILDING MANAGEMENT AND OPERATION</span>
                <span class="jp">建物総合管理事業</span>
              </h3>
              <div class="section-service--cnt-pickup-imgBox sp-only2">
	              <div class="section-service--cnt-pickup-img01">
	                <img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/top/service01.png" alt="">
	              </div>
	              <div class="section-service--cnt-pickup-img02">
	                <img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/top/service02.png" alt="">
	              </div>
	              <div class="section-service--cnt-pickup-img03">
	                <img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/top/service03.png" alt="">
	              </div>
	            </div>
              <div class="section-service--cnt-pickup-infor">
                <p class="desc">設備業務、清掃業務、駐車場管理、警備業務など、 ビルの総合メンテナンスを行っています。</p>
                <div class="_direct"><a href="" class="view-more">READ MORE</a></div>
              </div>
            </div>
            <div class="section-service--cnt-pickup-imgBox wow fadeInRight pc-only2">
              <div class="section-service--cnt-pickup-img01">
                <img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/top/service01.png" alt="">
              </div>
              <div class="section-service--cnt-pickup-img02">
                <img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/top/service02.png" alt="">
              </div>
              <div class="section-service--cnt-pickup-img03">
                <img class="cover border-radius-10" src="<?php echo $PATH;?>/assets/images/top/service03.png" alt="">
              </div>
            </div>
          </div>
          <div class="section-service--cnt-list">
            <div class="section-service--cnt-item wow fadeInLeft">
              <a href="" class="_wrap border-radius-10 link">
                <div class="section-service--cnt-item-inner">
                  <h3 class="section-ttl2">
                    <span class="en white">ROAD COMPREHENSIVE MANAGEMENT BUSINESS</span>
                    <span class="jp white">道路総合管理事業</span>
                  </h3>
                  <div class="section-service--cnt-item-infor">
                    <p class="desc">道路清掃、道路保全、下水道整備、造園・緑化工事など 道路メンテナンスを行っています。</p>
                    <div class="section-service--cnt-item-direct align-center pc-only2">
                      <span class="view-more white">READ MORE</span>
                    </div>
                  </div>
                </div>
              </a>
            </div>
            <div class="section-service--cnt-item type2 wow fadeInRight">
              <a href="" class="_wrap border-radius-10 link">
                <div class="section-service--cnt-item-inner">
                  <h3 class="section-ttl2">
                    <span class="en white">EQUIPMENT CONSTRUCTION BUSINESS</span>
                    <span class="jp white">設備修繕工事事業</span>
                  </h3>
                  <div class="section-service--cnt-item-infor">
                    <p class="desc">電源さえあれば、器具を必要としないで点灯可能なLED蛍光灯 「ワンダーエコライト」</p>
                    <div class="section-service--cnt-item-direct align-center pc-only2">
                      <span class="view-more white">READ MORE</span>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section><!-- ./section-service -->
  <section class="section-sdgs">
    <div class="section-sdgs--inner">
      <div class="container2">
        <div class="section-sdgs--ttlWrap wow fadeInUp">
          <h2 class="section-ttl">
            <span class="en">SUSTAINABILITY</span>
            <span class="jp">SDGsへの取り組み</span>
          </h2>
        </div>
        <div class="section-sdgs--head wow fadeInUp">
          <div class="section-sdgs--head-intro">
            <div class="section-sdgs--head-icon">
              <img class="" src="<?php echo $PATH;?>/assets/images/top/icon-sdgs.png" alt="">
            </div>
            <div class="section-sdgs--head-cnt">
              <p class="desc">キョーエーメックはSDGsの掲げる目標に賛同し、 社会、地域、人、環境の持続可能な発展に貢献するため様々な活動に取り組み、 横浜市が評価する制度「横浜型地域貢献企業」に認定されました。</p>
            </div>
          </div>
          <div class="section-sdgs--head-direct">
            <a href="" class="btn-blue">詳しくはこちら</a>
          </div>
        </div>
        <div class="section-sdgs--cnt">
          <div class="section-sdgs--cnt-item wow fadeInLeft">
            <a href="" class="section-sdgs--cnt-item-inner link border-radius-10">
              <div class="section-sdgs--cnt-item-infor">
                <h3 class="section-sdgs--cnt-item-infor-ttl">
                  <span class="jp">会社情報</span>
                  <span class="en">COMPANY</span>
                </h3>
              </div>
              <div class="section-sdgs--cnt-item-icon">
                <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-arrow-right-white.svg" alt="">
              </div>
              <img class="cover" src="<?php echo $PATH;?>/assets/images/top/sdgs01.png" alt="">
            </a>
          </div>
          <div class="section-sdgs--cnt-item wow fadeInRight">
            <a href="" class="section-sdgs--cnt-item-inner link border-radius-10">
              <div class="section-sdgs--cnt-item-infor">
                <h3 class="section-sdgs--cnt-item-infor-ttl recruit">
                  <span class="jp">会社情報</span>
                  <span class="en">COMPANY</span>
                </h3>
              </div>
              <div class="section-sdgs--cnt-item-icon">
                <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-arrow-right-white.svg" alt="">
              </div>
              <img class="cover" src="<?php echo $PATH;?>/assets/images/top/sdgs02.png" alt="">
            </a>
          </div>
        </div>
        <section class="section-news wow fadeInUp">
          <div class="section-news--head">
            <h3 class="section-news--ttl">NEWS RELEASE</h3>
            <a href="" class="view-more2 pc-only2">一覧を見る</a>
          </div>
          <ul class="section-news--list">
            <li class="section-news--item">
              <a href="" class="section-news--item-inner">
                <div class="section-news--item-dateWrap">
                  <span class="date">2021.00.00</span>
                  <span class="tag">お知らせ</span>
                </div>
                <p class="desc">新着情報が掲載されます。</p>
              </a>
            </li>
            <li class="section-news--item">
              <a href="" class="section-news--item-inner">
                <div class="section-news--item-dateWrap">
                  <span class="date">2021.00.00</span>
                  <span class="tag">SGDs</span>
                </div>
                <p class="desc">新着情報が掲載されます。</p>
              </a>
            </li>
            <li class="section-news--item">
              <a href="" class="section-news--item-inner">
                <div class="section-news--item-dateWrap">
                  <span class="date">2021.00.00</span>
                  <span class="tag">お知らせ</span>
                </div>
                <p class="desc">新着情報が掲載されます。</p>
              </a>
            </li>
          </ul>
          <div class="sp-only2 align-center mgt-40">
          	<a href="" class="view-more2">一覧を見る</a>
          </div>
        </section><!-- ./section-news -->
      </div>
    </div>
  </section><!-- ./section-sdgs -->
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>