<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <section class="p-end--mv">
    <h2 class="p-end--ttl">
      <span class="en">CONTACT</span>
      <span class="jp">お問い合わせ</span>
    </h2>
  </section><!-- ./p-end--mv -->
  <div class="breadcrumbWrap">
    <div class="container">
      <div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li>お問い合わせ</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="p-end--cnt">
    <div class="container">
      <div class="contact-content">
        <div class="v-contact">
          <div class="u-layout-smaller">
            <div class="c-steps">
              <div class="c-steps__col">
                <span class="c-steps__col__number u-font-rajdhani">1</span>
                <p></p>
                <p class="c-steps__col__label">問い合わせ内容入力</p>
                <p></p>
              </div>
              <div class="c-steps__col is-active">
                <span class="c-steps__col__number u-font-rajdhani">2</span>
                <p></p>
                <p class="c-steps__col__label">内容確認</p>
                <p></p>
              </div>
              <div class="c-steps__col">
                <span class="c-steps__col__number u-font-rajdhani">3</span>
                <p></p>
                <p class="c-steps__col__label">完了</p>
                <p></p>
              </div>
            </div>
            <p class="c-contact__message">入力内容をご確認ください。 入力内容を編集する場合は「戻る」、 <br>お問い合わせを送る場合は「送信する」をクリックしてください。</p>
            <div id="mw_wp_form_mw-wp-form-215" class="mw_wp_form mw_wp_form_input">
              <form method="post" action="" enctype="multipart/form-data">
                <div class="c-form">
                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">お問い合わせ区分</span>
                    </label>
                    <div class="c-form__row__field type2">
                      お仕事のご依頼・ご相談 <input type="hidden" name="fullname" value="お仕事のご依頼・ご相談">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">会社名・団体名</span>
                    </label>
                    <div class="c-form__row__field type2">株式会社キョーエーメック</div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">お名前</span>
                    </label>
                    <div class="c-form__row__field type2">
                      山田太郎 <input type="hidden" name="fullname" value="山田太郎">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">フリガナ</span>
                    </label>
                    <div class="c-form__row__field type2">
                      ヤマダタロウ <input type="hidden" name="fullname" value="ヤマダタロウ">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">住所</span>
                    </label>
                    <div class="c-form__row__field type2">
                      <ul class="c-form__row-list type2">
                        <li>1010001</li>
                        <li>東京都</li>
                        <li>港区三田2-7-13</li>
                        <li>TDS三田ビル7F</li>
                      </ul>
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">電話番号</span>
                    </label>
                    <div class="c-form__row__field type2">
                      0312345678 <input type="hidden" name="fullname" value="0312345678">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">メールアドレス</span>
                    </label>
                    <div class="c-form__row__field type2">
                      example@xxxxxx.co.jp <input type="hidden" name="fullname" value="example@xxxxxx.co.jp">
                    </div>
                  </div>

                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">お問い合わせ内容</span>
                    </label>
                    <div class="c-form__row__field type2">
                      いつもお世話になっております。 株式会社〇〇　〇〇部　山田太郎です。 この度、貴社にビルメンテナンスの依頼を検討しており、一度相談させていただければと思いご連絡いたしました。 ご挨拶も兼ねてお打ち合わせの場を頂ければと存じますが、ご都合のほどいかがでしょうか。 お忙しいところ恐縮ですが、ご返答いただけますと幸いです。 どうぞよろしくお願いいたします。 <input type="hidden" name="fullname" value="いつもお世話になっております。 株式会社〇〇　〇〇部　山田太郎です。 この度、貴社にビルメンテナンスの依頼を検討しており、一度相談させていただければと思いご連絡いたしました。 ご挨拶も兼ねてお打ち合わせの場を頂ければと存じますが、ご都合のほどいかがでしょうか。 お忙しいところ恐縮ですが、ご返答いただけますと幸いです。 どうぞよろしくお願いいたします。">
                    </div>
                  </div>
                </div>
                <ul class="c-contact__action">
                  <li>
                    <a href="/" class="mg-auto btn-white">戻る</a>
                  </li>
                  <li>
                    <a href="/" class="mg-auto btn-blue">送信する</a>
                  </li>
                </ul>
                <input type="hidden" id="mw_wp_form_token" name="mw_wp_form_token" value="5f44e21288"><input type="hidden" name="_wp_http_referer" value="/contact/"><input type="hidden" name="mw-wp-form-form-id" value="215"><input type="hidden" name="mw-wp-form-form-verify-token" value="5568ba116390563b279f106b7571c3e8be9dee21">
              </form>
              <!-- end .mw_wp_form -->
            </div>
          </div>
        </div><!-- ./v-contact -->
      </div><!-- ./contact-content -->
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>