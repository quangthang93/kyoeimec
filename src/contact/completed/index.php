<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <section class="p-end--mv">
    <h2 class="p-end--ttl">
      <span class="en">CONTACT</span>
      <span class="jp">お問い合わせ</span>
    </h2>
  </section><!-- ./p-end--mv -->
  <div class="breadcrumbWrap">
    <div class="container">
      <div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li>お問い合わせ</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="p-end--cnt">
    <div class="container">
      <div class="contact-content">
        <div class="v-contact">
          <div class="u-layout-smaller type2">
            <div class="c-steps">
              <div class="c-steps__col">
                <span class="c-steps__col__number u-font-rajdhani">1</span>
                <p></p>
                <p class="c-steps__col__label">お客様情報を入力</p>
                <p></p>
              </div>
              <div class="c-steps__col">
                <span class="c-steps__col__number u-font-rajdhani">2</span>
                <p></p>
                <p class="c-steps__col__label">内容確認</p>
                <p></p>
              </div>
              <div class="c-steps__col is-active">
                <span class="c-steps__col__number u-font-rajdhani">3</span>
                <p></p>
                <p class="c-steps__col__label">完了</p>
                <p></p>
              </div>
            </div>

            <p class="title-lv3 align-center mgb-35">お問い合わせいただきありがとうございます。</p>
            <p class="desc2">お送りいただいた内容は担当者が確認し、改めてご連絡いたします。 もしお問い合わせから5営業日経っても連絡がない場合は、 お手数ですが改めてお問い合わせください。</p>
            <div class="align-center mgt-50">
              <a href="/" class="mg-auto btn-blue">トップページへ戻る</a>
            </div>
          </div>
        </div><!-- ./v-contact -->
      </div><!-- ./contact-content -->
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>